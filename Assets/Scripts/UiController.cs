using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour
{
    // Start is called before the first frame update
    public Text lifes;
    public Text mobsKilled;
    public Text score;
    private Player player;
    private GameManager gm;
    public Text finalKills;
    public Text finalScore;
    public Button again;

    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        player = GameObject.Find("Player").GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        lifes.text ="Health: "+ player.life + " %";
        mobsKilled.text = "Mobs Killed: " + gm.kills;
        score.text = "Score: " + gm.score+" pts";
        finalKills.text = "You killed: " + gm.kills + " Mobs";
        finalScore.text = " Your final score is: " + gm.score + " pts";
        again.onClick.AddListener(Again);

    }
    void Again() {
        SceneManager.LoadScene("Game");

    }
}

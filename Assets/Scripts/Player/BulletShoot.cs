﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShoot : MonoBehaviour
{
    // Start is called before the first frame update

    public float speed = 40f;
    public Rigidbody2D rb;
    int damage = 1;
    private GameManager gm;
    // Update is called once per frame
    void Start()
    {
       
        rb.velocity = transform.right * speed;
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        
    }
    private void Update()
    {
        if (transform.position.x > 10f|| transform.position.x<-10f || transform.position.y > 10f || transform.position.y < -10f )
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {

        Enemy enemy = collider.GetComponent<Enemy>();
        if (collider.CompareTag("Enemy"));
        { 
            enemy.TakeDamage(damage);
            gm.monsterkilled(1);
            gm.sumarScore(5);
            
            
        }
        Destroy(gameObject);

    }

}

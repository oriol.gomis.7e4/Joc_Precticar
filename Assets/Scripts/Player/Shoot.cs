using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shoot : MonoBehaviour
{
    public Transform aim;
    private Camera cam;
    public float speed = 20f;
    public GameObject bulletprefab;
    private Player player;

    // Start is called before the first frame update
    void Start()
    {
        
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        apuntarArma();
        disparar();
    }
    private void apuntarArma()
    {
        // Funcio que s'ha de aplicar a un objecte que faci d'arma, el que fa es agafar la posicio del mouse i apuntar cap a ella .
        Vector3 mouseposition = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector3 aimdirection = (mouseposition - transform.position).normalized;
        float angle = Mathf.Atan2(aimdirection.y, aimdirection.x) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0, 0, angle);
        
    }
    private void disparar()
    {
        if (Input.GetMouseButtonDown(0))
        {


            GameObject bullet = Instantiate(bulletprefab, transform.position, transform.rotation);

        }
    }

}

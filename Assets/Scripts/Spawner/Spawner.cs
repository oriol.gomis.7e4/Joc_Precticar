using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameObject enemy;
    private Player player;
    
    int time = 1;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<Player>();

        if (!player.isdead == true)
        {
            InvokeRepeating("CreateEnemy", 1, time);
        }
      
        

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void CreateEnemy()
    {

        if (!player.isdead == true)
        {
            Instantiate(enemy, new Vector3(transform.position.x, transform.position.y), Quaternion.identity);

        }
    }
}

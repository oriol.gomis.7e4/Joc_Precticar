using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    private Player player;
    public GameObject spawn;
    private int Score;
    private int mobsKilled;
    public GameObject result;
    void Start()
    {
        Score = 0;
        mobsKilled = 0;
        player = GameObject.Find("Player").GetComponent<Player>();
        result.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.isdead==true) {
            spawn.SetActive(false);
        }
    }
    public int score
    {
        get
        {
            return this.Score;

        }

    }
    public int kills
    {
        get
        {
            return this.mobsKilled;

        }
    }
    public void sumarScore(int score)
    {
        Score += score;

    }
    public void restarVida(int vida)
    {
        player.life -= vida;
        if (player.life==0) {
            player.isdead = true;
            player.die();
            result.SetActive(true);
        }

    }
    public void monsterkilled(int num)
    {
        mobsKilled += num;
    }



}
